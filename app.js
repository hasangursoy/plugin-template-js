const robomotion = require('./lib/robomotion/app');
const logging = require('./lib/robomotion/logging');
const messaging = require('./lib/robomotion/messaging');
const clipboard = require('./clipboard')

try {
    classes = Object.getOwnPropertyNames(clipboard);
    classes.forEach((c) => {
        func = Object.getOwnPropertyNames(clipboard[c]).filter((p) => {
            return typeof(clipboard[c][p]) === 'function' && p == 'Init';
        });
    
        clipboard[c][func]();
    });
    
    robomotion.Run();
} catch(ex) {
    messaging.NFactories().forEach(nf => {
        let guid = nf.Node.props != null ? nf.Node.props.Guid : "";
        logging.DebugError('{%s}.Plugin run failed: {%s}', guid, ex);
    });
}