const node = require('./lib/robomotion/node');

class Variable {
    constructor(scope='', name=''){
        this.scope = scope;
        this.name = name;
    }
}

class NodeProps extends node.INodeProps {
    constructor() {
        super();
        this.variable = new Variable();
    }
}

class Get extends node.Node {
    constructor() {
        super();
        this.NODE_NAME = 'Plugin.Clipboard.Get';
        this.props = new NodeProps();
    }

    static Init() {
        var aNode = new Get();

        aNode.OnCreateCallback = (data) => {
            try {
                aNode.props.variable = data.variable;
                aNode.OnCreateCB();
            } catch(ex) {
                throw(ex);
            }
            
        }
        aNode.OnMessageCallback = (data) => {
            try {
                return aNode.OnMessageCB(data);
            } catch(ex) {
                throw(ex);
            }
        }
        aNode.OnCloseCallback = () => {
            try {
                aNode.OnCloseCB();
            } catch(ex) {
                throw(ex);
            }
            
        }
        new node.NodeFactory(aNode, aNode.NODE_NAME);
    }
	
	OnCreateCB() {
		// TODO
	}
	
	OnMessageCB(data) {
        var msg = JSON.parse(data);
        msg.payload = "js plugin";
        return JSON.stringify(msg);
	}
	
	OnCloseCB() {
		// TODO
	}
}

module.exports = {Get};