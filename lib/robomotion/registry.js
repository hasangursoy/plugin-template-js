const messaging = require('./messaging');
const factory = require('./factory');
const logging = require('./logging');
const events = require('./events');

class Registry {
    static RegisterNode(name, fact){
        var connection = messaging.Connection();
        var nf = new factory(fact);
        connection.subscribe(name, (msg) => {
            nf.Handler(msg);
        });
    }

    static CreateNode(guid, name, node){
        var connection = messaging.Connection();
        var n = new NodeHandler(name, guid, 0, node);

        var subject = n.Subject();
        n.subscription = connection.subscribe(subject, null, (msg, reply, subject, sid) => {
            n.Handler(msg, subject)
        });
    }
}

class NodeHandler {
    constructor(name, guid, subscription, handler) {
        this.name = name;
        this.guid = guid;
        this.subscription = subscription;
        this.handler = handler;
    }

    Subject() {
        return this.guid+'.'+'>';
    }

    Handler(msg, subject) {
        var guid = subject.split('.')[0];
        var method = subject.split('.')[1];
        
        switch (method) {
            case 'OnClose':
                try {
                    logging.DebugLog('%s on close', this.name);
                    var connection = messaging.Connection();
                    connection.unsubscribe(this.subscription);
                    this.handler.OnClose();
                } catch(ex) {
                    events.EmitError(guid, ex);
                }
                break;
            case 'OnMessage':
                logging.DebugLog('%s on message started', this.name);
                try {
                    this.handler.OnMessage(msg, (data) => {
                        logging.DebugLog('%s on message finished', this.name);
                        events.EmitOutput(guid, data, 0);
                    });
                } catch(ex) {
                    events.EmitError(guid, ex);
                }
                break;
            default:
                events.EmitError(guid, "nodeHandler: unknown node");
                break;
        }
    }
}

module.exports = Registry;