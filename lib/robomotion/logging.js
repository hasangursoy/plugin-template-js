const sprintf = require('sprintf-js').sprintf

const ErrorPrefix = '[ERROR]'
const InfoPrefix = '[INFO]'
const LogPrefix = '[LOG]'


class Logging {
    static DebugError(format, ...args){
        Debugger.Debug(ErrorPrefix, format, ...args);
    }

    static DebugInfo(format, ...args){
        Debugger.Debug(InfoPrefix, format, ...args);
    }

    static DebugLog(format, ...args){
        Debugger.Debug(LogPrefix, format, ...args);
    }
}

class Debugger {
    static Debug(prefix, format, ...args) {
        format = sprintf('%s %s', prefix, format);
        console.log(sprintf(format, ...args));
    }
}


module.exports = Logging;