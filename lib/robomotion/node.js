const messaging = require('./messaging');
const registry = require('./registry');
const events = require('./events');

class NodeFactory {
    constructor(node, nodeName){
        this.node = node;
        this.nodeName = nodeName;
        messaging.NFactories().push(this);
    }

    Register() {
        registry.RegisterNode(this.nodeName, this);
    }

    OnCreate(config) {
        try {
            messaging.SetActiveNodes(messaging.ActiveNodes() + 1);
            var json = JSON.parse(config);
            this.node.props = new INodeProps(json.guid, json.name, json.delayBefore, json.delayAfter);
            this.node.OnCreateCallback(json);
            registry.CreateNode(this.node.props.guid, this.node.props.name, this.node);
        } catch(ex) {
            let guid = this.node.props != null ? this.Node.props.Guid : "";
            events.EmitError(guid, ex);
            process.exit(1);
        }
    }
}

class INodeProps {
    constructor(guid='', name='', delayBefore=0.0, delayAfter=0.0) {
        this.guid = guid;
        this.name = name;
        this.delayBefore = delayBefore;
        this.delayAfter = delayAfter;
    }
}

class Node {
    constructor(guid='', name='', delayBefore=0, delayAfter=0) {
        this.props = new INodeProps();
        this.props.guid = guid;
        this.props.name = name;
        this.props.delayBefore = delayBefore;
        this.props.delayAfter = delayAfter;
        this.OnCreateCallback = (data) => {};
        this.OnMessageCallback = (data) => {};
        this.OnCloseCallback = () => {};
    }

    OnMessage(inMsg, handlerFunc) {
        try {
            this.Sleep(this.delayBefore * 1000);
            var outMsg = this.OnMessageCallback(inMsg);
            this.Sleep(this.delayAfter * 1000);
            handlerFunc(outMsg);
        } catch(ex) {
            throw(ex);
        }

        /*
        setTimeout(() => {
            try {
                var outMsg = this.OnMessageCallback(inMsg);
                setTimeout(() => {
                    handlerFunc(outMsg);
                }, (this.delayAfter) * 1000);
            } catch(ex) {
                throw(ex);
            }

        }, this.delayBefore * 1000);
        */
    }

    OnClose() {
        try {
            this.OnCloseCallback();
            messaging.SetActiveNodes(messaging.ActiveNodes() - 1);
            if (messaging.ActiveNodes() == 0) {
                process.exit(1);
            }
        } catch(ex) {
            throw(ex);
        }
    }

    Sleep(ms){
        var waitTill = new Date(new Date().getTime() + ms);
        while(waitTill > new Date()){}
    }
}

module.exports = {NodeFactory, INodeProps, Node};