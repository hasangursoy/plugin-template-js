class Messaging {
    constructor() {
        this.connection = null;
        this.nodeMap = null;
        this.nFactories = new Array();
        this.activeNodes = 0;
    }
    
    static Connection() {
        return this.connection;
    }

    static NodeMap() {
        return this.nodeMap;
    }

    static NFactories() {
        if (!this.nFactories) {
            this.nFactories = new Array();
        }
        return this.nFactories;
    }

    static ActiveNodes() {
        return this.activeNodes;
    }

    static SetConnection(con) {
        this.connection = con;
    }

    static SetNodeMap(map) {
        this.nodeMap = map;
    }

    static SetActiveNodes(count) {
        this.activeNodes = count;
    }

    static Send(subject, data) {
        let con = this.Connection();
        con.publish(subject, data, function() {
        });

        con.on('error', function(e) {
            console.log('Error [' + con.currentServer + ']: ' + e);
        });
    }
}

module.exports = Messaging;