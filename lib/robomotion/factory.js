class Factory {
    constructor(fact){
        this.handler = fact;
    }

    Handler(msg) {
        this.handler.OnCreate(msg);
    }
}

module.exports = Factory;