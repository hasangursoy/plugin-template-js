const nats = require('nats');
const messaging = require('./messaging');
const logging = require('./logging');
const url = nats.DEFAULT_URI;

class App {
    static Run(){
        // Connect to NATS server.
        const nc = nats.connect(url);
        messaging.SetConnection(nc);
        messaging.SetNodeMap(new Map());
        messaging.SetActiveNodes(0);

        nc.on('connect', function() {
            nc.subscribe("wires", function(msg) {
                if (!msg) {
                    messaging.NodeMap().clear();
                } else {
                    let flow = JSON.parse(msg);
                    messaging.NodeMap().set(flow.Guid, flow.Wires);
                }
            });

            messaging.NFactories().forEach(fact => {
                fact.Register();
            });
        });
        
        logging.DebugInfo('Started');

        nc.on('error', function(e) {
            logging.DebugError('Error [%s]: %s', nc.currentServer, e)
            process.exit();
        });

        nc.on('close', function() {
            logging.DebugError('Closed')
            process.exit();
        });
    }
}

module.exports = App;