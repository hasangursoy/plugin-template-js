const messaging = require('./messaging');
const logging = require('./logging');

const EType = {
    CREATE  : 0,
    INPUT   : 1,
    OUTPUT  : 2,
    ERROR   : 3,
    CLOSE   : 4,
    LOG     : 5,
    FLOW    : 6,
    DEBUG   : 7
}

const FlowEvent = {
    SEND    : 'send',
    RCV     : 'receive',
    ERROR   : 'error',
    START   : 'start',
    STOP    : 'stop',
}

class EventData {
    constructor(messageId, type, nodeid, payload, timestamp) {
        this.messageId = messageId;
        this.type = type;
        this.nodeid = nodeid;
        this.payload = payload;
        this.timestamp = timestamp;
    }
}

class Events {
    static EmitFlowEvent(guid, name) {
        let e = new EventData('', 'event', guid, name, (new Date).getTime());
        let data = JSON.stringify(e);
        this.EmitEvent(EType.FLOW, guid, data);
    }

    static EmitError(guid, error) {
        logging.DebugError('%s.%s', guid, error);
    }

    static EmitOutput(guid, output, port) {
        if (!messaging.NodeMap().has(guid)) {
            return;
        }

        let wires = messaging.NodeMap().get(guid)
        //let data = JSON.stringify(output);

        wires[port].forEach(wire => {
            messaging.Send(wire+'.OnMessage', output);
        });
        this.EmitEvent(EType.OUTPUT, guid, output);
    }

    static EmitEvent(type, subject, data) {
        switch (type) {
            case EType.CREATE:
                messaging.Send(subject, data);
                break;
            case EType.INPUT:
                messaging.Send(subject+'.OnMessage', data);
                break;
            case EType.OUTPUT:
                this.EmitFlowEvent(subject, FlowEvent.SEND);
                break;
            case EType.FLOW:
                messaging.Send('FlowEvent', data);
                break;
            case EType.DEBUG:
            case EType.LOG:
                break;
            case EType.ERROR:
                this.EmitFlowEvent(subject, FlowEvent.ERROR);
                break;
            case EType.CLOSE:
                messaging.NodeMap().keys().forEach(guid => {
                    messaging.Send(guid+'.OnClose', {});
                });
                messaging.NodeMap().clear();
                break;
        }
    }
}

module.exports = Events;